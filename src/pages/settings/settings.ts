import {Component} from '@angular/core';
import {LoadingController, MenuController, ToastController} from "ionic-angular";
import {MediasService} from "../../services/medias.service";

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  
  constructor(private menuCtrl: MenuController,
              private mediaService: MediasService,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController) {}
  
  onToggleMenu() {
    this.menuCtrl.open();
  }
  
  onSaveList() {
    let loader = this.loadingCtrl.create({
      content: 'Saving medias...'
    });
    loader.present();
    
    this.mediaService.saveBooksData().then(
      () => {
      },
      (error) => {
        loader.dismiss();
        console.log(error)
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'bottom'
        }).present();
      }
    );
  
    this.mediaService.saveCdsData().then(
      () => {
        loader.dismiss();
        this.toastCtrl.create({
          message: 'All medias have been saved successfully !!',
          duration: 3000,
          position: 'bottom'
        }).present();
      },
      (error) => {
        loader.dismiss();
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'bottom'
        }).present();
      }
    );
  }
  
  onFetchList() {
    let loader = this.loadingCtrl.create({
      content: 'Fetching medias...'
    });
    loader.present();
    
    this.mediaService.retrieveBooksData().then(
      () => {
        this.mediaService.saveBooksListToDevice();
      },
      (error) => {
        loader.dismiss();
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'bottom'
        }).present();
      }
    );
  
    this.mediaService.retrieveCdsData().then(
      () => {
        loader.dismiss();
        this.toastCtrl.create({
          message: 'All medias have been retrieved successfully !',
          duration: 3000,
          position: 'bottom'
        }).present();
        this.mediaService.saveCdsListToDevice();
      },
      (error) => {
        loader.dismiss();
        this.toastCtrl.create({
          message: error,
          duration: 3000,
          position: 'bottom'
        }).present();
      }
    );
  }
}
