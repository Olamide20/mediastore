import {Component, OnDestroy, OnInit} from "@angular/core";
import {MediasService} from "../../../services/medias.service";
import {Book} from "../../../models/Book";
import {MenuController, ModalController} from "ionic-angular";
import {LendBookPage} from "../lend-book/lend-book";
import {Subscription} from "rxjs";

@Component({
  selector: 'page-book-book-list',
  templateUrl: 'book-list.html'
})

export class BookListPage implements OnInit, OnDestroy{
  
  bookList: Book[];
  bookSubscription: Subscription;
  
  constructor (private mediaService: MediasService,
               private menuCtrl: MenuController,
               private modalCtrl: ModalController){}
  
  ngOnInit(): void {
    this.bookSubscription = this.mediaService.bookSubject$.subscribe(
      (book: Book[]) => {
        this.bookList = book.slice();
      }
    );
    this.mediaService.emitBooks();
  }
  
  ionViewWillEnter() {
    this.bookList = this.mediaService.bookList.slice();
  }
  
  onToggleMenu() {
    this.menuCtrl.open();
  }
  
  onLoadBook(i: number) {
    let modal = this.modalCtrl.create(LendBookPage, {index: i});
    modal.present();
  }
  
  ngOnDestroy() {
    this.bookSubscription.unsubscribe();
  }
}
