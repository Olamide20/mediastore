import {Component, OnInit} from "@angular/core";
import {Book} from "../../../models/Book";
import {NavParams, ViewController} from "ionic-angular";
import {MediasService} from "../../../services/medias.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'page-book-lend-book',
  templateUrl: 'lend-book.html'
})

export class LendBookPage implements OnInit{
  
  index: number;
  book: Book;
  lendBookForm: FormGroup;
  show = false;
  
  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private mediaService: MediasService,
              private formBuilder: FormBuilder) {
  }
  
  ngOnInit() {
    this.index = this.navParams.get('index');
    this.book = this.mediaService.bookList[this.index];
    this.initForm();
  }
  
  dismissModal() {
    this.viewCtrl.dismiss();
  }
  
  initForm() {
    this.lendBookForm = this.formBuilder.group({
      lendTo: ['', [Validators.required]]
    });
  }
  
  onLendBook(book: Book) {
    this.mediaService.lendOrReturn(book);
    this.mediaService.emitBooks();
    this.mediaService.saveBooksListToDevice()
  }
  
  onSubmitForm() {
    this.book.lendTo = this.lendBookForm.get('lendTo').value;
    this.onLendBook(this.book);
    this.dismissModal();
  }
  
  showForm() {
    this.show = true;
  }
}
