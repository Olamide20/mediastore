import {Component, OnDestroy, OnInit} from "@angular/core";
import {MediasService} from "../../../services/medias.service";
import {MenuController, ModalController} from "ionic-angular";
import {Cd} from "../../../models/Cd";
import {LendCdPage} from "../lend-cd/lend-cd";
import {Subscription} from "rxjs";

@Component({
  selector: 'page-cd-cd-list',
  templateUrl: 'cd-list.html'
})

export class CdListPage implements OnInit, OnDestroy{
  
  cdList: Cd[];
  cdSubscription: Subscription;
  
  constructor (private mediaService: MediasService,
               private menuCtrl: MenuController,
               private modalCtrl: ModalController){}
  
  ngOnInit(): void {
    this.cdSubscription = this.mediaService.cdSubject$.subscribe(
      (cd: Cd[]) => {
        this.cdList = cd.slice();
      }
    );
    this.mediaService.emitCds();
  }
  
  ionViewWillEnter() {
    this.cdList = this.mediaService.cdList.slice();
  }
  
  onToggleMenu() {
    this.menuCtrl.open();
  }
  
  onLoadCd(i: number) {
    let modal = this.modalCtrl.create(LendCdPage, {index: i});
    modal.present();
  }
  
  ngOnDestroy() {
    this.cdSubscription.unsubscribe();
  }
}
