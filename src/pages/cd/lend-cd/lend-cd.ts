import {Component, OnInit} from "@angular/core";
import {NavParams, ViewController} from "ionic-angular";
import {MediasService} from "../../../services/medias.service";
import {Cd} from "../../../models/Cd";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'page-cd-lend-cd',
  templateUrl: 'lend-cd.html'
})

export class LendCdPage implements OnInit{
  
  index: number;
  cd: Cd;
  lendCdForm: FormGroup;
  show = false;
  
  constructor(private navParams: NavParams,
              private viewCtrl: ViewController,
              private mediaService: MediasService,
              private formBuilder: FormBuilder) {
  }
  
  ngOnInit() {
    this.index = this.navParams.get('index');
    this.cd = this.mediaService.cdList[this.index];
    this.initForm();
  }
  
  initForm() {
    this.lendCdForm = this.formBuilder.group({
      lendTo: ['', [Validators.required]]
    });
  }
  
  dismissModal() {
    this.viewCtrl.dismiss();
  }

  onLendCd(cd: Cd) {
    this.mediaService.lendOrReturn(cd);
    this.mediaService.emitCds();
    this.mediaService.saveCdsListToDevice()
  }
  
  onSubmitForm() {
    this.cd.lendTo = this.lendCdForm.get('lendTo').value;
    this.onLendCd(this.cd);
    this.dismissModal();
  }
  
  showForm() {
    this.show = true;
  }
}
