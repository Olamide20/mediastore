import {Book} from "../models/Book";
import {Cd} from "../models/Cd";
import {Media} from "../models/Media";
import {Subject} from "rxjs";
import * as firebase from "firebase";
import {Injectable} from "@angular/core";
import {Storage} from "@ionic/storage";
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable()
export class MediasService{
  
  cdSubject$ = new Subject<Cd[]>();
  
  bookSubject$ = new Subject<Book[]>();
  
  bookList: Book[] = [];
  
  cdList: Cd[] = [];
  
  constructor(private storage: Storage) {}
  
  saveBooksListToDevice() {
    this.storage.set('books', this.bookList);
  }
  
  saveCdsListToDevice() {
    this.storage.set('cds', this.cdList);
  }
  
  fetchBookListFromDevice() {
    this.storage.get('books').then(
      (list) => {
        if (list && list.length) {
          this.bookList = list.slice();
        }
        this.emitBooks();
      }
    );
  }
  
  fetchCdListFromDevice() {
    this.storage.get('cds').then(
      (list) => {
        if (list && list.length) {
          this.cdList = list.slice();
        }
        this.emitCds();
      }
    );
  }
  
  lendOrReturn(media: Media){
    media.isLend = !media.isLend;
  }
  
  emitCds() {
    this.cdSubject$.next(this.cdList.slice());
  }
  
  emitBooks() {
    this.bookSubject$.next(this.bookList.slice());
  }
  
  saveBooksData() {
    return new Promise((resolve, reject) => {
      firebase.database().ref('books').set(this.bookList).then(
        (data: DataSnapshot) => {
          resolve(data);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
  
  retrieveBooksData() {
    return new Promise((resolve, reject) => {
      firebase.database().ref('books').once('value').then(
        (data: DataSnapshot) => {
          this.bookList = data.val();
          this.emitBooks();
          resolve('Données récupérées avec succès !');
        }, (error) => {
          reject(error);
        }
      );
    });
  }
  
  saveCdsData() {
    return new Promise((resolve, reject) => {
      firebase.database().ref('cds').set(this.cdList).then(
        (data: DataSnapshot) => {
          resolve(data);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }
  
  retrieveCdsData() {
    return new Promise((resolve, reject) => {
      firebase.database().ref('cds').once('value').then(
        (data: DataSnapshot) => {
          this.cdList = data.val();
          this.emitCds();
          resolve('Données récupérées avec succès !');
        }, (error) => {
          reject(error);
        }
      );
    });
  }
}
