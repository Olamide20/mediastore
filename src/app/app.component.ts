import {Component, OnInit, ViewChild} from '@angular/core';
import {MenuController, NavController, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {TabsPage} from "../pages/tabs/tabs";
import {SettingsPage} from "../pages/settings/settings";
import * as firebase from "firebase";
import {AuthPage} from "../pages/auth/auth";
import {MediasService} from "../services/medias.service";

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
  
  tabsPage:any = TabsPage;
  settingPage:any = SettingsPage;
  authPage:any = AuthPage;
  @ViewChild('content') content: NavController;
  isAuth: boolean;
  
  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private menuCtrl: MenuController,
              private mediaService: MediasService) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      let firebaseConfig = {
        apiKey: "AIzaSyBsCX4xBdxSqRjYxdoSR6X4I1aV4zpIWzo",
        authDomain: "mediastore-ab6b0.firebaseapp.com",
        databaseURL: "https://mediastore-ab6b0.firebaseio.com",
        projectId: "mediastore-ab6b0",
        storageBucket: "mediastore-ab6b0.appspot.com",
        messagingSenderId: "353196396048",
        appId: "1:353196396048:web:5f47c95656478b7b"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
      firebase.auth().onAuthStateChanged(
        (user) => {
          if (user) {
            this.isAuth = true;
            this.content.setRoot(TabsPage);
          } else {
            this.isAuth = false;
            this.content.setRoot(AuthPage, {mode: 'connect'});
          }
        });
    });
  }
  
  ngOnInit(): void {
    this.mediaService.fetchBookListFromDevice();
    this.mediaService.fetchCdListFromDevice();
  }
  
  onNavigate(page: any, data?: {}) {
    this.content.setRoot(page, data ? data : null);
    this.menuCtrl.close();
  }
  
  onDisconnect() {
    firebase.auth().signOut();
    this.menuCtrl.close();
  }
}

