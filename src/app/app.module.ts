import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {TabsPage} from "../pages/tabs/tabs";
import {MediasService} from "../services/medias.service";
import {BookListPage} from "../pages/book/book-list/book-list";
import {CdListPage} from "../pages/cd/cd-list/cd-list";
import {SettingsPage} from "../pages/settings/settings";
import {LendBookPage} from "../pages/book/lend-book/lend-book";
import {LendCdPage} from "../pages/cd/lend-cd/lend-cd";
import {AuthService} from "../services/auth.service";
import {AuthPage} from "../pages/auth/auth";
import {File} from "@ionic-native/file";
import {IonicStorageModule} from "@ionic/storage";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    BookListPage,
    CdListPage,
    SettingsPage,
    LendBookPage,
    LendCdPage,
    AuthPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    BookListPage,
    CdListPage,
    SettingsPage,
    LendBookPage,
    LendCdPage,
    AuthPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    MediasService,
    AuthService,
    File,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
