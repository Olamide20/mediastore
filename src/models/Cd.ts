import {Media} from "./Media";

export class Cd extends Media{
  
  constructor( title: string, public anneeSortie: string, isLend: boolean, lendTo: string) {
    super(title, lendTo, isLend);
  }
}
