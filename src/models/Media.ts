export class Media{
  
  constructor(public title: string,
              public lendTo: string,
              public isLend: boolean) { }
}
