import {Media} from "./Media";

export class Book extends Media{

  constructor( title: string, public author: string, isLend: boolean, lendTo: string) {
    super(title, lendTo,isLend);
  }
}
